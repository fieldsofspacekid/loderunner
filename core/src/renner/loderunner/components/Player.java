package renner.loderunner.components;

import com.artemis.Component;

import java.util.ArrayList;

public class Player extends Component {
    public enum PlayerState{STANDING, RUNNING, CLIMBING, LADDERSHIFTING, HANGING, FALLING, HIDING, GLUETHROWING, TRAPSETTING, TELEPORTING, DEAD}
    public enum TileType{EMPTY, PLATFORM, LADDER}

    public PlayerState playerState = PlayerState.STANDING;
    public float speed = .05f;

    //controls
    public boolean left, right, up, down;
    public boolean justLeft, justRight, justUp, justDown;

    //adjacent tiles
    public boolean[] adjPlatforms = new boolean[9];
    public boolean[] adjLadders = new boolean[9];

    //character tiles
    public boolean leftFootPlatform, rightFootPlatform, leftFootLadder, rightFootLadder;
    public boolean leftHandPlatform, rightHandPlatform, leftHandLadder, rightHandLadder;
    public boolean centerLadder;
    public boolean belowLadder, belowPlatform;
    public boolean abovePlatform;
}
