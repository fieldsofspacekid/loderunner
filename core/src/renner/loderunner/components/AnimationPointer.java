package renner.loderunner.components;

import com.artemis.Component;

public class AnimationPointer extends Component {
    public String animationId = "";
    public boolean flipX = false, flipY = false;
}
