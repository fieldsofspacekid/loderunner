package renner.loderunner.components;


import com.artemis.Component;
import com.badlogic.gdx.math.Vector3;

public class Transform extends Component {
    public Vector3 position = new Vector3();
    public float width = 1;
    public float height = 1;
}
