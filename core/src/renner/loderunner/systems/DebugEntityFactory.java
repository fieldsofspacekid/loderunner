package renner.loderunner.systems;

import com.artemis.Aspect;
import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import renner.loderunner.components.AnimationPointer;
import renner.loderunner.components.Player;
import renner.loderunner.components.Transform;

import java.util.HashMap;

@Wire
public class DebugEntityFactory extends BaseSystem {

    private ComponentMapper<Transform> transformComponentMapper;
    private ComponentMapper<AnimationPointer> animationPointerComponentMapper;
    private ComponentMapper<Player> playerComponentMapper;

    @Override
    protected void processSystem() {

    }

    @Override
    protected void initialize(){
        Entity e = this.world.createEntity();

        Transform t = transformComponentMapper.create(e);
        t.position = new Vector3(1,14,0);
        t.width = .5f;
        t.height = 1;

        AnimationPointer s = animationPointerComponentMapper.create(e);
        s.animationId = AnimationSystem.AnimPlayerStandingRight;

        Player pc = playerComponentMapper.create(e);
    }
}
