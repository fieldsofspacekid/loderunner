package renner.loderunner.systems;

import com.artemis.BaseSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class CameraSystem extends BaseSystem {

    public OrthographicCamera camera;

    @Override
    protected void initialize(){
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        float scale = 20;
        camera = new OrthographicCamera();
        camera.setToOrtho(false,(w / h)*scale, scale);
        camera.update();
    }


    @Override
    protected void begin(){
        Gdx.gl20.glClearColor(0.3f, 0.3f, 0.3f, 1.f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl20.glEnable(GL20.GL_BLEND);
        camera.update();
    }


    @Override
    protected void processSystem() {}

    @Override
    protected void end() {}

}