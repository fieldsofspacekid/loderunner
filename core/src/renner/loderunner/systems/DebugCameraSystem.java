package renner.loderunner.systems;

import com.artemis.BaseSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class DebugCameraSystem extends BaseSystem {

    //debug
    private BitmapFont font;
    private SpriteBatch batch;
    private String debugText = "";
    private OrthographicCamera debugCamera;


    public void setDebugText(String text){
        debugText = text;
    }

    @Override
    protected void initialize(){
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        debugCamera = new OrthographicCamera();
        debugCamera.setToOrtho(false,w,h);
        debugCamera.update();

        font = new BitmapFont();
        batch = new SpriteBatch();
    }


    @Override
    protected void begin(){
        debugCamera.update();
        batch.setProjectionMatrix(debugCamera.combined);
        batch.begin();
    }


    @Override
    protected void processSystem() {
        font.draw(batch, debugText, 5, font.getCapHeight()+5);
    }

    @Override
    protected void end() {
        batch.end();

    }



}