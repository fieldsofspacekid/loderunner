package renner.loderunner.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import renner.loderunner.components.AnimationPointer;
import renner.loderunner.components.Player;
import renner.loderunner.components.Transform;

@Wire
public class PlayerAnimationSystem extends EntityProcessingSystem {
    ComponentMapper<Transform> transformComponentMapper;
    ComponentMapper<Player> playerComponentMapper;
    ComponentMapper<AnimationPointer> animationPointerComponentMapper;

    public PlayerAnimationSystem() {
        super(Aspect.all(Player.class, Transform.class, AnimationPointer.class));
    }

    @Override
    protected void process(Entity e) {
        Player p = playerComponentMapper.get(e);
        Transform t = transformComponentMapper.get(e);
        AnimationPointer a = animationPointerComponentMapper.get(e);

        switch (p.playerState){

            case STANDING:
                a.animationId = AnimationSystem.AnimPlayerStandingRight;
                break;
            case RUNNING:
                if (p.left) {
                    a.animationId = AnimationSystem.AnimPlayerWalkingRight;
                    a.flipX = true;
                } else {
                    a.animationId = AnimationSystem.AnimPlayerWalkingRight;
                    a.flipX = false;
                }
                break;
            case CLIMBING:
                a.animationId = AnimationSystem.AnimPlayerStandingRight;
                break;
            case HANGING:
                break;
            case FALLING:
                a.animationId = AnimationSystem.AnimPlayerStandingRight;
                break;
            case DEAD:
                break;
        }


        /*//fall if needed
        switch (pc.playerState){
            case STANDING:
                a.animationId = AnimationSystem.AnimPlayerStandingRight;

                if (pc.left || pc.right) {
                    a.animationId = AnimationSystem.AnimPlayerWalkingRight;
                    if (pc.left) {
                        a.flipX = true;
                        if (!leftHandPlatform) {
                            t.position.add(-pc.speed, 0, 0);
                        }
                    } else {
                        a.flipX = false;
                        if (!rightHandPlatform) {
                            t.position.add(pc.speed, 0, 0);
                        }
                    }
                }
                break;
            case RUNNING:

                if (pc.left || pc.right) {
                    a.animationId = AnimationSystem.AnimPlayerWalkingRight;
                    if (pc.left) {
                        a.flipX = true;
                        if (!leftHandPlatform) {
                            t.position.add(-pc.speed, 0, 0);
                        }
                    } else {
                        a.flipX = false;
                        if (!rightHandPlatform) {
                            t.position.add(pc.speed, 0, 0);
                        }
                    }
                } else {
                   pc.playerState = Player.PlayerState.STANDING;
                }
                break;
            case CLIMBING:
                break;
            case HANGING:
                break;
            case FALLING:
                a.animationId = AnimationSystem.AnimPlayerStandingRight; //falling animation
                t.position.add(0, -pc.speed * 2, 0);

                if (hasGround){
                    pc.playerState = Player.PlayerState.STANDING;
                } else {
                    pc.playerState = Player.PlayerState.FALLING;
                }
                break;
            case DEAD:
                break;
        }*/
      /*  if (hasLadder){
            if (pc.up || pc.down){
                a.animationId = AnimationSystem.AnimPlayerStandingRight;
                t.position.x = (int) (t.position.x + t.width/2);
                if (pc.up){ //todo head hit platform?
                    t.position.add(0, pc.speed, 0);
                } else { //todo feet hit platform?
                    t.position.add(0, -pc.speed, 0);
                }
            }
        }*/

          /*  //fall if needed
            if (hasGround) {
                t.position.add(0, -pc.speed * 2, 0);
            } else {

                //handle animations
                if (pc.left || pc.right) {
                    a.animationId = AnimationSystem.AnimPlayerWalkingRight;
                    if (pc.left) {
                        a.flipX = true;
                        if (!leftHandPlatform) {
                            t.position.add(-pc.speed, 0, 0);
                        }
                    } else {
                        a.flipX = false;
                        if (!rightHandPlatform) {
                            t.position.add(pc.speed, 0, 0);
                        }
                    }
                } else {
                    a.animationId = AnimationSystem.AnimPlayerStandingRight;
                }
            }*/

    }

}
