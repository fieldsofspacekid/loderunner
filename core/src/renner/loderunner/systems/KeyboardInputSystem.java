package renner.loderunner.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import renner.loderunner.components.Player;

@Wire
public class KeyboardInputSystem extends EntityProcessingSystem {

    ComponentMapper<Player> mPlayerControlled;

    public KeyboardInputSystem() {
        super(Aspect.all(Player.class));
    }

    @Override
    protected void process(Entity e) {
        Player playerControlled = mPlayerControlled.get(e);

        playerControlled.up = Gdx.input.isKeyPressed(Input.Keys.UP);
        playerControlled.left = Gdx.input.isKeyPressed(Input.Keys.LEFT);
        playerControlled.right = Gdx.input.isKeyPressed(Input.Keys.RIGHT);
        playerControlled.down = Gdx.input.isKeyPressed(Input.Keys.DOWN);

        playerControlled.justUp = Gdx.input.isKeyJustPressed(Input.Keys.UP);
        playerControlled.justLeft = Gdx.input.isKeyJustPressed(Input.Keys.LEFT);
        playerControlled.justRight = Gdx.input.isKeyJustPressed(Input.Keys.RIGHT);
        playerControlled.justDown = Gdx.input.isKeyJustPressed(Input.Keys.DOWN);
    }

}
