package renner.loderunner.systems;

import com.artemis.BaseSystem;
import com.artemis.annotations.Wire;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import renner.loderunner.MainGame;
import renner.loderunner.components.Player;

import java.util.ArrayList;

@Wire
public class TiledMapSystem extends BaseSystem {
    public  static final String MapLayerPlatforms = "Platforms";
    public  static final String MapLayerLadders = "Ladders";

    private TiledMap tiledMap;
    private TiledMapRenderer tiledMapRenderer;

    private CameraSystem cameraSystem;
    private DebugCameraSystem debugCameraSystem;

    //derived
    public float tileWidth, tileHeight, unitScale;

    public TiledMapSystem(String tilemapPath){
        tiledMap = new TmxMapLoader().load(tilemapPath);
        TiledMapTileLayer layer = ((TiledMapTileLayer)tiledMap.getLayers().get(0));
        tileWidth = layer.getTileWidth();
        tileHeight = layer.getTileHeight();
        unitScale = 1/tileWidth;

        System.out.println(tileWidth);

        tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap, unitScale);
    }

    @Override
    protected void processSystem() {
        tiledMapRenderer.setView(cameraSystem.camera);
        tiledMapRenderer.render();

        if (MainGame.debug) {
            Vector3 mouseInView = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            Vector3 mouseInWorld = cameraSystem.camera.unproject(mouseInView); //view to world space

            int row = (int) (mouseInWorld.y);
            int col = (int) (mouseInWorld.x);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(col).append(", ").append(row);
            stringBuilder.append("(").append(mouseInWorld.x).append(", ").append(mouseInWorld.y).append(")");
            debugCameraSystem.setDebugText(stringBuilder.toString());
        }
    }

    public void setCell(final int x, final int y, TiledMapTileLayer.Cell cell, String layerName){
        TiledMapTileLayer layer = (TiledMapTileLayer)tiledMap.getLayers().get(layerName);
        layer.setCell(x, y, cell);
    }

   /* public HashSet<TiledMapTileLayer.Cell> getTiles(final int minX, final int minY, final int maxX, final int maxY){
        HashSet<TiledMapTileLayer.Cell> result = new HashSet<>();
        TiledMapTileLayer layer = (TiledMapTileLayer)tiledMap.getLayers().get(MapLayerPlatforms);
        for (int y=minY; y<maxY; y++){
            for (int x=minX; x<maxX; x++){
                TiledMapTileLayer.Cell cell = layer.getCell(x,y);
                if (cell !=null){
                    result.add(cell);
                }
            }
        }

        return result;
    }*/

    public TiledMapTileLayer.Cell getCellAt(final int x, final int y, String layerName){
        TiledMapTileLayer layer = (TiledMapTileLayer)tiledMap.getLayers().get(layerName);
        return layer.getCell(x,y);
    }

    public ArrayList<TiledMapTileLayer.Cell> getSurroundingCells(final int x, final int y, String layerName){
        TiledMapTileLayer layer = (TiledMapTileLayer)tiledMap.getLayers().get(layerName);
        ArrayList<TiledMapTileLayer.Cell> cells = new ArrayList<>(9);
        cells.add(0, layer.getCell(x-1, y+1)); //ul
        cells.add(1, layer.getCell(x, y+1));      //t
        cells.add(2, layer.getCell(x+1, y+1)); //ur
        cells.add(3, layer.getCell(x+1, y));      //r
        cells.add(4, layer.getCell(x+1, y-1)); //lr
        cells.add(5, layer.getCell(x, y-1));      //b
        cells.add(6, layer.getCell(x-1, y-1)); //ll
        cells.add(7, layer.getCell(x-1, y));      //l
        cells.add(8, layer.getCell(x-1, y));      //l

        return cells;
    }

   /* public ArrayList<Player.TileType> getTileTypesByTile(final int x, final int y) {
        ArrayList<Player.TileType> algamanation =new ArrayList<>(9);

        ArrayList<Player.TileType> platforms = getTileTypesByTile(x, y, MapLayerPlatforms, Player.TileType.PLATFORM);
        ArrayList<Player.TileType> ladders = getTileTypesByTile(x, y, MapLayerLadders, Player.TileType.LADDER);

        for (int i=0; i<platforms.size(); i++){
            if (platforms.get(i) != Player.TileType.EMPTY){
                algamanation.add(Player.TileType.PLATFORM);
                continue;
            }


            if (ladders.get(i) != Player.TileType.EMPTY){
                algamanation.add(Player.TileType.LADDER);
                continue;
            }

            algamanation.add(Player.TileType.EMPTY);

        }
        return algamanation;
    }*/

    public boolean[] getTileTypesByTile(final int x, final int y, String layerName){
        boolean[] tileTypeArray = new boolean[9];
        TiledMapTileLayer layer = (TiledMapTileLayer)tiledMap.getLayers().get(layerName);

        int i=0;
        for (int row=-1; row<2; row++){
            for (int col=-1; col<2; col++){
                tileTypeArray[i] = layer.getCell(x+col, y+row) != null;
                i++;
            }
        }

        return tileTypeArray;
    }

   /* public ArrayList<Player.TileType> getTileTypesByRect(final float x, final float y, final float width, final float height, final String layerName, final Player.TileType type){
        ArrayList<Player.TileType> tileTypeList = new ArrayList<>();
        TiledMapTileLayer layer = (TiledMapTileLayer)tiledMap.getLayers().get(layerName);

        for (int row=-1; row<2; row++){
            for (int col=-1; col<2; col++){
                //TODO
                tileTypeList.add(layer.getCell(x+col, y+row) != null ? type : Player.TileType.EMPTY);
            }
        }

        return tileTypeList;
    }*/

    @Override
    protected void dispose(){
        tiledMap.dispose();
    }
}
