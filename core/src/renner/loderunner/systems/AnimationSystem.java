package renner.loderunner.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import renner.loderunner.components.AnimationPointer;
import renner.loderunner.components.Player;
import renner.loderunner.components.Transform;

import java.util.HashMap;

@Wire
public class AnimationSystem extends EntityProcessingSystem {
    public static final String AnimPlayerWalkingRight = "player_walking";
    public static final String AnimPlayerStandingRight = "player_standing";

    private ComponentMapper<Transform> transformComponentMapper;
    private ComponentMapper<AnimationPointer> animationPointerComponentMapper;
    private ComponentMapper<Player> playerComponentMapper;

    private CameraSystem cameraSystem;

    private HashMap<String, Animation<TextureRegion>> animationMap = new HashMap<>();
    private SpriteBatch sb;

    private float time = 0f;

    public AnimationSystem() {
        super(Aspect.all(Transform.class, AnimationPointer.class));

        sb = new SpriteBatch();
        this.loadAnimations();
    }

    private void loadAnimations(){

        //Atlas
        TextureAtlas textureAtlas = new TextureAtlas("animations/characters.txt");

        //Walking Right
        String[] RUNNER_RUNNING_REGION_NAMES = new String[] {"alienGreen_run1", "alienGreen_run2"};

        TextureRegion[] runningFrames = new TextureRegion[RUNNER_RUNNING_REGION_NAMES.length];
        for (int i=0; i< RUNNER_RUNNING_REGION_NAMES.length; i++){
            String path = RUNNER_RUNNING_REGION_NAMES[i];
            runningFrames[i] = textureAtlas.findRegion(path);
        }
        Animation<TextureRegion> walkAnimation = new Animation(.1f, runningFrames);
        animationMap.put(AnimPlayerWalkingRight, walkAnimation);

        //Standing
        TextureRegion[] standingFrames = new TextureRegion[1];
        standingFrames[0] = textureAtlas.findRegion("alienGreen_jump");
        Animation<TextureRegion> standingAnimation = new Animation(.1f, standingFrames);
        animationMap.put(AnimPlayerStandingRight, standingAnimation);
    }

    @Override
    protected void initialize(){}

    @Override
    protected void begin(){
        sb.setProjectionMatrix(cameraSystem.camera.combined);
        sb.begin();
        time += this.getWorld().getDelta();
    }

    @Override
    protected void process(Entity e) {
        Transform t = transformComponentMapper.get(e);
        AnimationPointer ap = animationPointerComponentMapper.get(e);


        if (animationMap.containsKey(ap.animationId)){
            Animation<TextureRegion> a = animationMap.get(ap.animationId);
            TextureRegion currentFrame = a.getKeyFrame(time, true);

            //flip if needed
            if ((ap.flipX && !currentFrame.isFlipX()) || (!ap.flipX && currentFrame.isFlipX())){
                currentFrame.flip(true, false);
            }
            if ((ap.flipY && !currentFrame.isFlipY()) || (!ap.flipY && currentFrame.isFlipY())){
                currentFrame.flip(false, true);
            }

            sb.draw(currentFrame, t.position.x, t.position.y, t.width, t.height); //todo put the size somewhere
        } else {
            //todo log the warning
        }

    }

    @Override
    protected void end(){
        sb.end();
    }
}
