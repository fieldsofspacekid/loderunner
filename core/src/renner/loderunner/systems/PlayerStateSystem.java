package renner.loderunner.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Wire;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;
import renner.loderunner.components.AnimationPointer;
import renner.loderunner.components.Player;
import renner.loderunner.components.Transform;

@Wire
public class PlayerStateSystem extends EntityProcessingSystem {
    private static final float FOOT_TOLERANCE = 0.0f;
    ComponentMapper<Transform> transformComponentMapper;
    ComponentMapper<Player> playerComponentMapper;

    TiledMapSystem tiledMapSystem;

    public PlayerStateSystem() {
        super(Aspect.all(Player.class, Transform.class, AnimationPointer.class));
    }

    @Override
    protected void process(Entity e) {
        Player p = playerComponentMapper.get(e);
        Transform t = transformComponentMapper.get(e);
        //AnimationPointer a = animationPointerComponentMapper.get(e);

        Vector2 leftFoot = new Vector2(t.position.x + FOOT_TOLERANCE, t.position.y);
        Vector2 rightFoot = new Vector2( t.position.x + t.width - FOOT_TOLERANCE, t.position.y);

        Vector2 leftHand = new Vector2(t.position.x, t.position.y +  t.height/2);
        Vector2 rightHand = new Vector2( t.position.x + t.width, t.position.y +  t.height/2);

        Vector2 above = new Vector2(t.position.x + t.width/2, t.position.y + t.height);
        Vector2 center = new Vector2(t.position.x + t.width/2, t.position.y + t.height/2);
        Vector2 below = new Vector2(t.position.x + t.width/2, t.position.y);


        p.adjPlatforms = tiledMapSystem.getTileTypesByTile((int) center.x, (int) center.y, TiledMapSystem.MapLayerPlatforms);


        p.leftHandPlatform = tiledMapSystem.getCellAt((int)leftHand.x, (int)leftHand.y, TiledMapSystem.MapLayerPlatforms) != null;
        p.rightHandPlatform = tiledMapSystem.getCellAt((int)rightHand.x, (int)rightHand.y, TiledMapSystem.MapLayerPlatforms) != null;

        p.leftFootPlatform = tiledMapSystem.getCellAt((int)leftFoot.x, (int)leftFoot.y, TiledMapSystem.MapLayerPlatforms) != null;
        p.rightFootPlatform = tiledMapSystem.getCellAt((int)rightFoot.x, (int)rightFoot.y, TiledMapSystem.MapLayerPlatforms) != null;

        p.leftFootLadder = tiledMapSystem.getCellAt((int)leftFoot.x, (int)leftFoot.y, TiledMapSystem.MapLayerLadders) != null;
        p.rightFootLadder = tiledMapSystem.getCellAt((int)rightFoot.x, (int)rightFoot.y, TiledMapSystem.MapLayerLadders) != null;

        p.rightHandLadder = tiledMapSystem.getCellAt((int)leftHand.x, (int)leftHand.y, TiledMapSystem.MapLayerLadders) != null;
        p.leftHandLadder = tiledMapSystem.getCellAt((int)rightHand.x, (int)rightHand.y, TiledMapSystem.MapLayerLadders) != null;

        p.centerLadder = tiledMapSystem.getCellAt((int)center.x, (int)center.y, TiledMapSystem.MapLayerLadders) != null;

        p.belowLadder = tiledMapSystem.getCellAt((int)below.x, (int)below.y, TiledMapSystem.MapLayerLadders) != null;

        p.belowPlatform = tiledMapSystem.getCellAt((int)below.x, (int)below.y, TiledMapSystem.MapLayerPlatforms) != null;
        p.abovePlatform = tiledMapSystem.getCellAt((int)above.x, (int)above.y, TiledMapSystem.MapLayerPlatforms) != null;
        //p.abovePlatform = tiledMapSystem.getCellAt((int)below.x, (int)below.y, TiledMapSystem.MapLayerPlatforms) != null;

        boolean hasGround = p.leftFootPlatform || p.rightFootPlatform || p.leftFootLadder || p.rightFootLadder;
        //TODO adjust speed if there is glue, so check for glue
        
        //transition to falling after running, hanging, climbing, or standing
        if (!hasGround){
            switch (p.playerState){
                case HANGING:
                case CLIMBING:
                case STANDING:
                case RUNNING:
                    p.playerState = Player.PlayerState.FALLING;
                    return; //can't do anything else, cause you are falling
            }
        }

        //transition to standing after falling
        if (hasGround){
            switch (p.playerState){
                case FALLING:
                    p.playerState = Player.PlayerState.STANDING;
                    break; //break since we may be able to run!
            }
        }

        if (p.right || p.left) {
            boolean canMove = (p.left && !p.leftHandPlatform) || (p.right && !p.rightHandPlatform);
            //transition to running from standing (or stop you if there is a wall)
            if (canMove) {
                switch (p.playerState) {
                    case RUNNING:
                        if (p.centerLadder){
                            p.playerState = Player.PlayerState.CLIMBING;
                        }
                        break;
                    case STANDING:
                        p.playerState = Player.PlayerState.RUNNING;
                        break;
                    case CLIMBING:
                        //TODO
                        break;
                    case HANGING:
                        //TODO
                        break;
                }
            } else { //can't move left or right!
                switch (p.playerState) {
                    case RUNNING:
                        p.playerState = Player.PlayerState.STANDING;
                        break;
                }
            }
        } else if (p.up || p.down){
            boolean canMove = (p.centerLadder || p.belowLadder);
            if (p.down && p.belowPlatform || p.up && p.abovePlatform){
                canMove = false;
            }
            if (canMove){
                switch (p.playerState){

                    case STANDING:
                        if (!(p.up && !p.centerLadder)){ //top of ladder
                            p.playerState = Player.PlayerState.CLIMBING;
                        }
                        break;
                    case RUNNING:
                    case HANGING:
                    case FALLING:
                        p.playerState = Player.PlayerState.CLIMBING;
                        break;
                }
            } else { //can't move up or down!
                switch (p.playerState){
                    case CLIMBING:
                        p.playerState = Player.PlayerState.STANDING;
                        break;
                }
            }
        } else { //not moving at all
            switch (p.playerState){
                case RUNNING:
                    p.playerState = Player.PlayerState.STANDING;
                    break;
            }
        }

    }

}
