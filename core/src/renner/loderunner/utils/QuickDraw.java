package renner.loderunner.utils;

import Jama.Matrix;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class QuickDraw {

    private static ShapeRenderer sr = new ShapeRenderer();

    private static SpriteBatch sb = new SpriteBatch();
    private static BitmapFont font = new BitmapFont();

    private static GlyphLayout layout = new GlyphLayout();

    public static void drawPolygonStroke(float[] pts, int strokeWidth, float[] color){
        Gdx.gl20.glLineWidth(strokeWidth);
        sr.setColor(color[0], color[1], color[2], color[3]);
        sr.begin(ShapeRenderer.ShapeType.Line);
        sr.polyline(pts, 0, pts.length);
        sr.end();
        Gdx.gl20.glLineWidth(1);
    }

    public static void drawPolygonFill(float[] pts, float[] color){
        sr.setColor(color[0], color[1], color[2], color[3]);
        sr.begin(ShapeRenderer.ShapeType.Filled);
        for (int i=0; i<pts.length/2-2; i++){
            sr.triangle(pts[0], pts[1], pts[i*2+2], pts[i*2+3], pts[i*2+4], pts[i*2+5]);
        }
        sr.end();
    }

    public static float[] toFloatArray(double[] arr) {
        if (arr == null) return null;
        int n = arr.length;
        float[] ret = new float[n];
        for (int i = 0; i < n; i++) {
            ret[i] = (float)arr[i];
        }
        return ret;
    }

    public static void drawPolygonText(float[] pos, float[] color, String text) {
        sb.begin();
        font.setColor(color[0], color[1], color[2], color[3]);
        layout.setText(font, text);
        //font.draw(sb, text, pos[0], pos[1]);
        font.draw(sb, layout, pos[0]-layout.width/2, pos[1] + layout.height/2);
        sb.end();
    }

    public static void drawDrawable(Matrix llur, Drawable d){
        sb.begin();
        Matrix dim = llur.getMatrix(0,1,1,1).minus(llur.getMatrix(0,1,0,0));
        d.draw(sb, (float)llur.get(0,0), (float)llur.get(1,0),(float)dim.get(0,0), (float)dim.get(1,0));
        sb.end();
    }

    public static void drawDrawable(float[] pos, Drawable d){
        sb.begin();
        d.draw(sb, pos[0], pos[1], d.getMinWidth(), d.getMinHeight());
        sb.end();
    }
}
