package renner.loderunner;

import com.artemis.World;
import com.artemis.WorldConfigurationBuilder;
import com.badlogic.gdx.ApplicationAdapter;
import renner.loderunner.systems.*;

public class MainGame extends ApplicationAdapter {
	public static final float delta = 1f/60f;
	public static final int gridSize = 25;
	private World world;

	public static boolean debug = true;

	public MainGame(){}

	@Override
	public void create () {
		WorldConfigurationBuilder builder = new WorldConfigurationBuilder()
				.with(new CameraSystem())
				.with(new TiledMapSystem("maps/level0.tmx"))
				.with(new AnimationSystem())

				.with(new KeyboardInputSystem())
				.with(new PlayerStateSystem())
				.with(new PlayerAnimationSystem())
				.with(new PlayerMovementSystem())
				.with(new DebugEntityFactory());

				if (debug) {
					builder.with(new DebugCameraSystem());
				}


		this.world = new World(builder.build());
	}

	@Override
	public void render () {
		this.world.setDelta(delta);
		this.world.process();

	}
	
	@Override
	public void dispose () {
	}
}
